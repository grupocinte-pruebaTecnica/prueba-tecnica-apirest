FROM openjdk:8

COPY ./target/prueba-tecnica-apirest-0.0.1-SNAPSHOT.jar /usr/app/

WORKDIR /usr/app

RUN sh -c 'touch /prueba-tecnica-apirest-0.0.1-SNAPSHOT.jar'

ENTRYPOINT ["java","-jar","prueba-tecnica-apirest-0.0.1-SNAPSHOT.jar"]