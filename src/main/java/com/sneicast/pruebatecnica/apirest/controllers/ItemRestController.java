/**
 * 
 */
package com.sneicast.pruebatecnica.apirest.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sneicast.pruebatecnica.apirest.models.Item;
import com.sneicast.pruebatecnica.apirest.services.ItemService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Sneider Castillo Gaona
 *
 */
@RestController
@RequestMapping("/api/item")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@Api(tags = "Item")
public class ItemRestController {

	@Autowired
	ItemService itemService;

	
	@GetMapping
	@ApiOperation(value = "Obtener items", notes = "Servicio que obtiene todos los items creados")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "lista de usuarios"), @ApiResponse(code = 404, message = "datos no encontrados")})
	public ResponseEntity<List<Item>> getAllItems(@RequestParam(required = false, defaultValue = "") String search) {
		List<Item> items = new ArrayList<>();
		HttpStatus httpStatus = HttpStatus.OK;

		try {
			items = itemService.getAllItems(search);
			if (items.size() == 0) {
				httpStatus = HttpStatus.NOT_FOUND;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(items, httpStatus);
	}

	@GetMapping(value = "/{id}")
	@ApiOperation(value = "detalle item", notes = "Servicio que obtiene el detalle de un item")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "detalle item"), @ApiResponse(code = 404, message = "item no encontrado")})
	public ResponseEntity<Item> detailItem(@PathVariable("id") String id) {
		HttpStatus httpStatus = HttpStatus.OK;

		Item itemDetail = null;
		try {
			itemDetail = itemService.detailItem(id);
			if(itemDetail == null) {
				httpStatus = HttpStatus.NOT_FOUND;
			}

		} catch (Exception e) {
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(itemDetail, httpStatus);
		
		
	}

	@PostMapping
	@ApiOperation(value = "Crear item", notes = "Servicio que permite crear un item")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Item creado"), @ApiResponse(code = 500, message = "Error al crear el item")})
	public ResponseEntity<Item> createItem(@RequestBody Item item) {

		HttpStatus httpStatus = HttpStatus.CREATED;
		Item  result = null;

		try {
			result = itemService.createItem(item);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(result, httpStatus);
	}

	@PutMapping(value = "/{id}")
	@ApiOperation(value = "Actualizar item", notes = "Servicio que permite actualizar un item")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Item actualizado"), @ApiResponse(code = 500, message = "Error al actualizar el item")})
	public ResponseEntity<Item> updateItem(@PathVariable("id") String id, @RequestBody Item item) {
		item.setId(id);
		
		HttpStatus httpStatus = HttpStatus.OK;
		Item  result = null;
		try {
			result = itemService.updateItem(item);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(result, httpStatus);
	}

	@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "Eliminar item", notes = "Servicio que permite eliminar un item")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Item Eliminado"), @ApiResponse(code = 500, message = "Error al eliminar el item")})
	public ResponseEntity<Item> deleteItem(@PathVariable("id") String id) {
		HttpStatus httpStatus = HttpStatus.OK;
		

		try {
			 itemService.deleteItem(id);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			
		}

		return new ResponseEntity<>(null, httpStatus);
	}

}
