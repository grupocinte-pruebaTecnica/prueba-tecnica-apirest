/**
 * 
 */
package com.sneicast.pruebatecnica.apirest.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sneicast.pruebatecnica.apirest.models.Item;

/**
 * @author Sneider Castillo Gaona
 *
 */
@Service
@Transactional(readOnly = true)
public class ItemService {

	private static final String TITLE_COLLECTION = "items";

	@Transactional
	public List<Item> getAllItems(String search) throws Exception {

		List<Item> list = new ArrayList<>();
		Firestore db = FirestoreClient.getFirestore();

		Query query = db.collection(TITLE_COLLECTION);// .whereEqualTo("id", search);
		ApiFuture<QuerySnapshot> querySnapshot = query.get();

		for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
			Item item = document.toObject(Item.class);
			list.add(item);
		}
		return list;
	}

	@Transactional
	public Item detailItem(String id) throws Exception {
		Item detail = null;
		Firestore db = FirestoreClient.getFirestore();
		
		ApiFuture<DocumentSnapshot> future = db.collection(TITLE_COLLECTION).document(id).get();

		DocumentSnapshot document = future.get();
		if (document.exists()) {
			detail = document.toObject(Item.class);
		} else {
			System.out.println("No se encontro el Item");
		}
		return detail;
	}

	@Transactional
	public Item createItem(Item item) throws Exception {
		Firestore db = FirestoreClient.getFirestore();
		ApiFuture<WriteResult> future = db.collection(TITLE_COLLECTION).document(item.getId()).set(item);
		
		return item;
	}

	@Transactional
	public Item updateItem(Item item) throws Exception {
		
		Firestore db = FirestoreClient.getFirestore();
		
		ApiFuture<WriteResult> future = db.collection(TITLE_COLLECTION).document(item.getId()).set(item);
		
		return item;
	}

	@Transactional
	public void deleteItem(String id) throws Exception {
		Firestore db = FirestoreClient.getFirestore();
		ApiFuture<WriteResult> future = db.collection(TITLE_COLLECTION).document(id).delete();
		
	}

}
