package com.sneicast.pruebatecnica.apirest;

import java.io.FileInputStream;
import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@SpringBootApplication
public class PruebaTecnicaApirestApplication {

	private static final String FB_BASE_URL = "https://pruebatecnicagrupocinte.firebaseio.com";
	
	public static void main(String[] args) {
		SpringApplication.run(PruebaTecnicaApirestApplication.class, args);
		
		/*inicializar firebase*/
				try {
					FirebaseOptions options = new FirebaseOptions.Builder()
					  .setCredentials(GoogleCredentials.fromStream(new ClassPathResource("google-services.json").getInputStream()))
					  .setDatabaseUrl(FB_BASE_URL)
					  .build();
					FirebaseApp.initializeApp(options);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				
				
				
				
				
	}

}
