/**
 * 
 */
package com.sneicast.pruebatecnica.apirest.models;

/**
 * @author Sneider Castillo gaona
 *
 */
public class Item {

	private String id;
	private String title;

	public Item() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
